all: build run

build:
	docker build -t mycrpd mycrpd

run:
	docker run -ti --rm --name mycrpd1 -d -v ${PWD}/mycrpd1:/config -p 830:830 mycrpd

show:
	python3 show.py

down:
	docker kill mycrpd1
