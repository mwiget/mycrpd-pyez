from jnpr.junos import Device
from pprint import pprint

print("get facts using username/password ...")
with Device(host='localhost', user='root', password='password1') as dev:   
    pprint (dev.facts)

print("get facts using username and ssh key ...")
with Device(host='localhost', user='root') as dev:   
    pprint (dev.facts)
