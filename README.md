
# crpd-pyez

Example to build augmented crpd with ssh enabled, using the users ~/ssh_rsa.pub key.

Build and run mycrpd1 with

```
$ make 
pyez$ make
cp ~/.ssh/id_rsa.pub mycrpd
docker build -t mycrpd mycrpd
Sending build context to Docker daemon  5.632kB
Step 1/8 : FROM crpd:19.4R1.10 AS build
 ---> 5b6acdd96efb
Step 2/8 : RUN apt-get update     && apt-get -y --no-install-recommends install ethtool
 ---> Using cache
 ---> 0bcd458dee64
Step 3/8 : FROM crpd:19.4R1.10
 ---> 5b6acdd96efb
Step 4/8 : COPY --from=build /sbin/ethtool /sbin/
 ---> Using cache
 ---> 291658b13684
Step 5/8 : COPY sshd_config /etc/ssh/
 ---> Using cache
 ---> f31fb953fe15
Step 6/8 : COPY id_rsa.pub /
 ---> Using cache
 ---> 50360b484652
Step 7/8 : COPY rc.conf.local /etc
 ---> Using cache
 ---> 678b275bbc24
Step 8/8 : RUN mkdir /root/.ssh && chmod 700 /root/.ssh     && cat /id_rsa.pub >> /root/.ssh/authorized_keys     && chmod 600 /root/.ssh/authorized_keys     && c
hmod 400 /etc/ssh/sshd_config     && ln -s  /lib/systemd/system/ssh.service /etc/systemd/system/default.target.wants/ssh.service
 ---> Using cache
 ---> 16050ed59404
Successfully built 16050ed59404
Successfully tagged mycrpd:latest
docker run -ti --rm --name mycrpd1 -d -v /home/mwiget/Dropbox/git/crpd-pyez/mycrpd1:/config -p 830:830 mycrpd
15c0a75bd953b60764a5e6c84e43a17376b4ef914c75c221ffd5979ac0e1c1aa
```

Check mycrpd1 is running:

```
$ docker ps 
CONTAINER ID        IMAGE               COMMAND                CREATED             STATUS              PORTS
                      NAMES
15c0a75bd953        mycrpd              "/sbin/runit-init 0"   36 seconds ago      Up 35 seconds       22/tcp, 179/tcp, 3784/tcp, 4784/tcp, 6784/tcp, 7784/tcp, 0
.0.0.0:830->830/tcp   mycrpd1
```

Set your ssh public key in mycrpd1/juniper.conf


Now run the pyez script show.py to retrieve facts:

```
$ python3 show.py
get facts using username/password ...
{'2RE': None,
 'HOME': None,
 'RE0': None,
 'RE1': None,
 'RE_hw_mi': None,
 'current_re': None,
 'domain': None,
 'fqdn': '32d2ca8531c3',
 'hostname': '32d2ca8531c3',
 'hostname_info': {'re0': '32d2ca8531c3'},
 'ifd_style': 'CLASSIC',
 'junos_info': None,
 'master': None,
 'model': 'CRPD',
 'model_info': {'re0': 'CRPD'},
 'personality': None,
 're_info': None,
 're_master': None,
 'serialnumber': None,
 'srx_cluster': None,
 'srx_cluster_id': None,
 'srx_cluster_redundancy_group': None,
 'switch_style': 'NONE',
 'vc_capable': False,
 'vc_fabric': None,
 'vc_master': None,
 'vc_mode': None,
 'version': '0.0I0.0',
 'version_RE0': None,
 'version_RE1': None,
 'version_info': junos.version_info(major=(0, 0), type=I, minor=0, build=0),
 'virtual': None}
get facts using username and ssh key ...
{'2RE': None,
 'HOME': None,
 'RE0': None,
 'RE1': None,
 'RE_hw_mi': None,
 'current_re': None,
 'domain': None,
 'fqdn': '32d2ca8531c3',
 'hostname': '32d2ca8531c3',
 'hostname_info': {'re0': '32d2ca8531c3'},
 'ifd_style': 'CLASSIC',
 'junos_info': None,
 'master': None,
 'model': 'CRPD',
 'model_info': {'re0': 'CRPD'},
 'personality': None,
 're_info': None,
 're_master': None,
 'serialnumber': None,
 'srx_cluster': None,
 'srx_cluster_id': None,
 'srx_cluster_redundancy_group': None,
 'switch_style': 'NONE',
 'vc_capable': False,
 'vc_fabric': None,
 'vc_master': None,
 'vc_mode': None,
 'version': '0.0I0.0',
 'version_RE0': None,
 'version_RE1': None,
 'version_info': junos.version_info(major=(0, 0), type=I, minor=0, build=0),
 'virtual': None}
```


Shutdown the container with

```
make down
```

or 

```
docker stop mycrpd1
```

## Notes

The example includes ethtool into mycrpd container. This is normally not required, except when building veth links between containers to disable tcp checksum offload via `docker exec mycrpd1 /sbin/ethtool --offload eth0 tx off`

## Issues

If you get a python error like 'not a valid RSA private key file pyez', then fix your ssh key:

```
ssh-keygen -p -m PEM -f ~/.ssh/id_rsa
```
